﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GetMontlyPremium.Models
{
    public class Occupation
    {
        public int ID { get; set; }
        public string OccupationName { get; set; }

    }

    public class MonthlyPremium
    {
        public int ID { get; set; }
        public long DeathCoverAmount { get; set; }
        public decimal Premium { get; set; }
    }

}
