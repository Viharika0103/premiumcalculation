﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GetMontlyPremium.Models;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json.Linq;

namespace GetMontlyPremium.Controllers
{

    public class HomeController : Controller
    {
        public IConfigurationRoot GetConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appSettings.json").Build();
            return builder;

        }
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOccupationList()
        {
            // Getting connecting string from appsettings.json
            SqlConnection con = new SqlConnection(GetConnection().GetSection("ConnectionStrings").GetSection("DBContext").Value);

            List<Occupation> Occlov = new List<Occupation>();
            Occupation objlov = new Occupation();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_GetOccupations", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dtoccupation = new DataTable();
                adpt.Fill(dtoccupation);
                con.Close();

                if (dtoccupation.Rows.Count > 0)
                {
                    for (int i = 0; i < dtoccupation.Rows.Count; i++)
                    {
                        objlov = new Occupation();
                        objlov.ID = Convert.ToInt32(dtoccupation.Rows[i]["ID"].ToString());
                        objlov.OccupationName = dtoccupation.Rows[i]["Occupation"].ToString();
                        Occlov.Add(objlov);
                    }

                }
            }
            catch (Exception)
            {

            }

            return new JsonResult(new SelectList(Occlov.ToArray(), "ID", "OccupationName"));
        }


        [HttpGet]
        public JsonResult GetMontlyPremium(long DeathCoverAmount, int Occupation, int Age)
        {
            List<MonthlyPremium> Mpremium = new List<MonthlyPremium>();
            MonthlyPremium objMP = new MonthlyPremium();

            // building Query string to Pass to API to get the Premium
            var builder = new UriBuilder($"http://localhost:50000/api/Premium");

            builder.Port = 5000;
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["DeathCoverAmount"] = DeathCoverAmount.ToString();
            query["OccupationID"] = Occupation.ToString();
            query["Age"] = Age.ToString();
            builder.Query = query.ToString();
            string url = builder.ToString();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://Localhost:50000/api/Premium");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                string GetJsonString = response.Content.ReadAsStringAsync().Result;
                JArray arry = JArray.Parse(GetJsonString);
                {
                    foreach (JObject o in arry.Children<JObject>())
                    {
                        objMP = new MonthlyPremium();
                        
                        objMP.DeathCoverAmount = Convert.ToInt32(o["deathCoverAmount"].ToString());
                        objMP.Premium = Convert.ToDecimal(o["premium"]);
                        Mpremium.Add(objMP);
                    }

                }
            }

            return new JsonResult(new SelectList(Mpremium.ToArray(), "ID", "Premium")); ;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
