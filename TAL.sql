USE [master]
GO
/****** Object:  Database [TAL]    Script Date: 24-01-2021 16:30:38 ******/
CREATE DATABASE [TAL]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TAL', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.VINITH\MSSQL\DATA\TAL.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TAL_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.VINITH\MSSQL\DATA\TAL_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TAL] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TAL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TAL] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TAL] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TAL] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TAL] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TAL] SET ARITHABORT OFF 
GO
ALTER DATABASE [TAL] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TAL] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TAL] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TAL] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TAL] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TAL] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TAL] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TAL] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TAL] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TAL] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TAL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TAL] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TAL] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TAL] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TAL] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TAL] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TAL] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TAL] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TAL] SET  MULTI_USER 
GO
ALTER DATABASE [TAL] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TAL] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TAL] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TAL] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TAL] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TAL]
GO
/****** Object:  Table [dbo].[Tal_M_Factor]    Script Date: 24-01-2021 16:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tal_M_Factor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [nvarchar](50) NULL,
	[Factor] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Tal_M_Factor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaL_M_Occupations]    Script Date: 24-01-2021 16:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaL_M_Occupations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Occupation] [nvarchar](100) NULL,
	[Rating] [nvarchar](100) NULL,
 CONSTRAINT [PK_TaL_M_Occupations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Tal_M_Factor] ON 

INSERT [dbo].[Tal_M_Factor] ([ID], [Rating], [Factor]) VALUES (1, N'Professional', CAST(1.00 AS Decimal(18, 2)))
INSERT [dbo].[Tal_M_Factor] ([ID], [Rating], [Factor]) VALUES (2, N'White Collar', CAST(1.25 AS Decimal(18, 2)))
INSERT [dbo].[Tal_M_Factor] ([ID], [Rating], [Factor]) VALUES (3, N'Light Manual', CAST(1.50 AS Decimal(18, 2)))
INSERT [dbo].[Tal_M_Factor] ([ID], [Rating], [Factor]) VALUES (4, N'Heavy Manual', CAST(1.75 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Tal_M_Factor] OFF
SET IDENTITY_INSERT [dbo].[TaL_M_Occupations] ON 

INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (1, N'Cleaner', N'Light Manual')
INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (2, N'Doctor', N'Professional')
INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (3, N'Author', N'White Collar')
INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (4, N'Farmer', N'Heavy Manual')
INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (5, N'Mechanic', N'Heavy Manual')
INSERT [dbo].[TaL_M_Occupations] ([ID], [Occupation], [Rating]) VALUES (6, N'Florist', N'Light Manual')
SET IDENTITY_INSERT [dbo].[TaL_M_Occupations] OFF
/****** Object:  StoredProcedure [dbo].[SP_GetDeathSumPremium]    Script Date: 24-01-2021 16:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetDeathSumPremium] 
	@DeathCoveramount int,
	@OccupationId int,
	@Age int
AS
BEGIN

-- decalring variables
declare @MonthdeathPremium decimal(18,2), @Factory decimal(18,2), @Rating nvarchar(100), @Occupation nvarchar(100)

-- setting the values to the varaibles
set @Occupation = (select [Occupation] from [dbo].[TaL_M_Occupations] where [ID]=@OccupationId)
set @Rating     = (select Rating from [dbo].[TaL_M_Occupations] where [ID]=@OccupationId)
set @Factory = (select [Factor] from [dbo].[Tal_M_Factor] where [Rating] = (select Rating from [dbo].[TaL_M_Occupations] where [ID]=@OccupationId))


-- Calculating Premium
set @MonthdeathPremium = (@DeathCoveramount * @Factory  * @Age) / (1000 * 12)

select  @DeathCoveramount as DeathCoverAmount,@Occupation as Occupation, @Rating as Rating,@Factory as Factory, @MonthdeathPremium as MonthlyPremium
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GetOccupations]    Script Date: 24-01-2021 16:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetOccupations] 
	
AS
BEGIN
	select [ID],[Occupation] from [dbo].[TaL_M_Occupations]
END

GO
USE [master]
GO
ALTER DATABASE [TAL] SET  READ_WRITE 
GO
