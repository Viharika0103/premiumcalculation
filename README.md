# Dev Environment Setup
- Install dotnetcore 3.1: https://www.microsoft.com/net/core#windows
- Install visual studio 2019 : https://visualstudio.microsoft.com/downloads/
- Install SQL Server 2016 : expression edition.

# Download and install all packages needed for front end
$ npm install

# Download required .Net libraries and build the code base
$ dotnet clean
$ dotnet restore
$ dotnet build

#Download NuGetPackages
System.Data.SqlClient
Microsoft.Extensions.Configuration
Newtonsoft.Json

# As the soulution has both api and mvc project to getmonth Premium make sure that you set the solution to run multiple startup projects

Solution Rightclick  Properties-> Startup Project  right side pane -> select multiple startup projects
#download post man to test only API
#Make sure you restrict the localhost port as we are running two projects at same time

#API URL http://localhost:5000/api/Premium
Pass the parameters to Query string
http://localhost:5000/api/Premium?DeathCoverAmount=500000&OccupationId= 2&Age=30


