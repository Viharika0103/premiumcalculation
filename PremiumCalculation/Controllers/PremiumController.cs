﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using PremiumCalculation.Models;
using Microsoft.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;


namespace PremiumCalculation.Controllers
{
    [Route("api/Premium")]
    [ApiController]
    public class PremiumController : ControllerBase
    {
        public IConfigurationRoot GetConnection()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appSettings.json").Build();
            return builder;

        }

        // GET: api/Premium?DeathCoverAmount=100000&OccupationId=1&Age=30
        #region GetMonthlyPremium
        [HttpGet]
        public IActionResult GetMonthlyPremium([FromQuery]CalPremium objcalPre)
        {
            // Getting connecting string from appsettings.json
            SqlConnection con = new SqlConnection(GetConnection().GetSection("ConnectionStrings").GetSection("DBContext").Value);  
            
            MonthlyPremium objpl = new MonthlyPremium();
            List<MonthlyPremium> MP = new List<MonthlyPremium>();

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_GetDeathSumPremium", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DeathCoveramount", objcalPre.DeathCoverAmount);
                cmd.Parameters.AddWithValue("@OccupationId", objcalPre.OccupationId);
                cmd.Parameters.AddWithValue("@Age", objcalPre.Age);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                
                DataTable dtpatient = new DataTable();
                adpt.Fill(dtpatient);
                con.Close();

                if (dtpatient.Rows.Count > 0)
                {
                    objpl = new MonthlyPremium();
                    objpl.DeathCoverAmount = Convert.ToUInt32(dtpatient.Rows[0]["DeathCoverAmount"].ToString());
                    objpl.Occupation = dtpatient.Rows[0]["Occupation"].ToString();
                    objpl.Rating = dtpatient.Rows[0]["Rating"].ToString();
                    objpl.Factory = dtpatient.Rows[0]["Factory"].ToString();
                    objpl.Premium = Convert.ToDecimal(dtpatient.Rows[0]["MonthlyPremium"].ToString());
                    objpl.Status = "OK";
                    MP.Add(objpl);

                }
                else
                {
                    objpl.DeathCoverAmount = 0;
                    objpl.Occupation = "0";
                    objpl.Rating = "0";
                    objpl.Factory = "0";
                    objpl.Premium = Convert.ToDecimal("0");
                    objpl.Status = "0";
                    MP.Add(objpl);
                }



            }

            catch (Exception )
            {
                objpl.DeathCoverAmount = -1;
                objpl.Occupation = "-1";
                objpl.Rating = "-1";
                objpl.Factory = "-1";
                objpl.Premium = Convert.ToDecimal("0"); ;
                objpl.Status = "-1";
                MP.Add(objpl);
            }



             var json = JsonConvert.SerializeObject(MP, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            

            return new JsonResult(MP);


            // return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json") };

        }
        #endregion GetMonthlyPremium
    }
}
