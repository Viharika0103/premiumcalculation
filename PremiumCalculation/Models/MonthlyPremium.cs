﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PremiumCalculation.Models
{
    public class MonthlyPremium
    {
        public long DeathCoverAmount { get; set; }
        public string Occupation { get; set; }
        public string Rating { get; set; }
        public string Factory { get; set; }
        public decimal Premium { get; set; }
        public string Status { get; set; }
    }

    public class CalPremium
    {
        public long DeathCoverAmount { get; set; }
        public int OccupationId { get; set; }
        public int Age { get; set; }
    }
}
